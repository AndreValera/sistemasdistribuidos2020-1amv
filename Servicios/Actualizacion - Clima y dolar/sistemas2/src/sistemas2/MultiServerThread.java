/*
    #23|2|hola|amigo|& -- Servicio propio separa cadenas en subcadenas de 3
    #CRY|2|LLAVE16CARACTERES|MENSAJE -- Servicio de encriptación
    #CLIMA#
    #DOLAR#
*/

package sistemas2;

import java.net.*;
import java.io.*;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import static org.apache.commons.codec.binary.Base64.encodeBase64;

public class MultiServerThread extends Thread {
   private Socket socket = null;

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {

      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut;
		
	     while((lineIn = entrada.readLine()) != null){
            System.out.println("Received: "+lineIn);
            escritor.flush();
            
            if(lineIn.equals("FIN")) {
                ServerMultiClient.NoClients--;
                break;
            } else if(lineIn.equals("#cuantos#")){
                escritor.println("Clientes conectados: " + ServerMultiClient.NoClients); 
                escritor.flush();
            } else if(lineIn.equals("#CLIMA#")) {
                directorio(2, null, escritor);
            } else if(lineIn.equals("#DOLAR#")) {
                directorio(3, null, escritor);
            }else if(lineIn.equals("")) {
                escritor.println("echo..."); 
                escritor.flush();
            } else {
                //Descomponer la cadena recibida
                //#23|2|hola|amigo|&
                //#CRY|2|LLAVE16CARACT|MENSAJE|&
                ArrayList<String> parts;
                parts = procesadortxt(lineIn);
                
                if(parts.get(0).equals("#") && parts.get(1).equals("&")) {
                    //Protocolo respetado
                    
                    try {
                        int service;
                        if(parts.get(2).equals("CRY")) {
                            service = 1;
                        } else {
                            service = Integer.valueOf(parts.get(2));
                        }
                        
                        int param = Integer.valueOf(parts.get(3));
                        
                        int datos = parts.size() - 4;
                        
                        if(datos == param) {
                            String[] msgs = new String[datos];
                            for (int i = 0; i < datos; i++) {
                                msgs[i] = parts.get(4+i);
                            }
                            //protocolo respetado
                            directorio(service, msgs, escritor);
                        } else {
                            escritor.println("Protocolo no respetado");
                            escritor.flush();
                        }
                        
                    } catch(NumberFormatException ex) {
                        escritor.println("Servicio y parametros enteros. "
                                                    + "Protocolo no respetado");
                        escritor.flush();
                    }

                } else {
                    escritor.println("echo... " + lineIn);
                    escritor.flush();
                }
                
                //escritor.println(parts);
                escritor.flush();
            } 
        }
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(Exception e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
   
   private ArrayList<String> procesadortxt(String request) {
       
        int itera = 0;
        int begin;
        boolean barra = false;
        ArrayList<String> process = new ArrayList();
        
        //obtiene el hash
        process.add(request.substring(0, 1));
        
        //obtiene el ampersand
        process.add(request.substring(request.length() - 1));
        
        
        //Obtenemos el numero de servicio
        for (int i = 0; i < request.length(); i++) {
            if(request.charAt(i) == '|') {
                barra = true;
                break;
            } else {
                itera++;
            }
        }
        if(barra) {
            process.add(request.substring(1, itera));
        } else {
            process.clear();
            process.add("error");
            return process;
        }
        
        //Obtenemos el numero de parametros a utilizar
        itera++;
        begin = itera;
        barra = false;
        
        for (int i = itera; i < request.length(); i++) {
           if(request.charAt(i) == '|') {
               barra = true;
               break;
           } else {
               itera++;
           }
        }
        if(barra) {
            process.add(request.substring(begin, itera));
        } else {
            process.clear();
            process.add("error");
            return process;
        }
        
        //Obtenemos los parametros a utilizar
        itera++;
        begin = itera;
        barra = false;
        
        while(itera < (request.length() - 1)) {
            for (int i = begin; i < request.length(); i++) {
                if(request.charAt(i) == '|') {
                    barra = true;
                    break;
                } else {
                    itera++;
                }
            }
            if(barra) {
                process.add(request.substring(begin,itera));
            } else {
                process.clear();
                process.add("error");
                return process; 
            }
            itera++;
            begin = itera;
            barra = false;
        }

        return process;
   }
   
   private void directorio(int dir, String[] msgs, PrintWriter writer) {
       String msgfinal;
       int cdatos;
       
       switch(dir) {
           case 1:
               //Servicio de encriptacion
               cdatos = msgs.length;
               msgfinal = "#R-CRY|1|";
               
               if(cdatos == 2) {
                   if(msgs[0].length() >= 16) {
                       //Sintaxis correcta
                       String key = msgs[0];
                       //String key = "92AE31A79FEEB2A3"; //llave
                       String iv = "0123456789ABCDEF"; // vector de inicialización
                       String enc = encriptado(key, iv, msgs[1]); 
                       msgfinal = msgfinal + enc + "|&";
                       writer.println(msgfinal);
                       writer.flush();
                   } else {
                       writer.println("Longitud minima de Llave para "
                                                + "encriptado AES: 16 bytes");
                       writer.flush();
                   }
                   
               } else {
                   writer.println("El servicio de encriptación solo debe llevar"
                           + "clave y mensaje. #CRY|2|LLAVE|MSG|&");
                   writer.flush();
               }
               break;
               
           case 2:
               //Servicio de clima
               msgfinal = "#R-CLIMA|3|";
               GetClima clima = new GetClima();
               msgfinal = msgfinal + clima.getDatos().get(0) + "|" +
                       clima.getDatos().get(1) + "|" + clima.getDatos().get(2) + "|&";
               writer.println(msgfinal);
               writer.flush();
               break;
               
           case 3:
               //Servicio del precio del dolar
               msgfinal = "#R-DOLAR|2|";
               GetDolarInfo dolar = new GetDolarInfo();
               msgfinal = msgfinal + dolar.getC1() + "|" + dolar.getV1() + "|&";
               writer.println(msgfinal);
               writer.flush();
               break;
           
           case 23:
                //Servicio 23 activado
               cdatos = 0;
               msgfinal = "#R-" + dir;

               for (int i = 0; i < msgs.length; i++) {
                   cdatos = cdatos + subStringC(msgs[i]).length;  
               }
               msgfinal = msgfinal + "|" + cdatos + "|";
               
               for (int i = 0; i < msgs.length; i++) {
                   String[] cads = subStringC(msgs[i]);
                   for (int j = 0; j < cads.length; j++) {
                       msgfinal = msgfinal + cads[j] + "|";
                   }
               }
               
               msgfinal = msgfinal + "&";
               writer.println(msgfinal);
               writer.flush();
                
               break;
           default:
                writer.println("Error: Servicio no disponible");
                writer.flush();
                break;
       }
   }
   
   private static String encriptado(String key, String iv, String cleartext) {
       try {
           // Definición del tipo de algoritmo a utilizar (AES, DES, RSA)
           String alg = "AES";
           // Definición del modo de cifrado a utilizar
           String cI = "AES/CBC/PKCS5Padding";
           
           Cipher cipher = Cipher.getInstance(cI);
           SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), alg);
           IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
           cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);
           byte[] encrypted = cipher.doFinal(cleartext.getBytes());
           //return new String(encodeBase64(encrypted));
           return new String(encodeBase64(encrypted));
       } catch (NoSuchAlgorithmException ex) {
           Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
       } catch (NoSuchPaddingException ex) {
           Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
       } catch (InvalidKeyException ex) {
           Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
       } catch (InvalidAlgorithmParameterException ex) {
           Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
       } catch (IllegalBlockSizeException ex) {
           Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
       } catch (BadPaddingException ex) {
           Logger.getLogger(MultiServerThread.class.getName()).log(Level.SEVERE, null, ex);
       }
       return "Error";
   }
   
   private String[] subStringC(String cad) {
       
       int cadlength = 3; //Tamaño maximo de cada sub cadena
       int cant = cad.length(); //Tamaño de la cadena original
       int div = cant / cadlength; //Cantidad de subcadenas completas
       int res = cant % cadlength; //Tamaño de la subcadena incompleta
       String[] subcads; //Subcadenas
       
       if(res != 0) {
           //Si hay una subcadena incompleta la agregamos a las completas
           subcads = new String[div + 1];
           subcads[div] = cad.substring(div * cadlength); //Ahorramos lineas de codigo agregado la subcadena incompleta al final
       } else {
           //Si no hay subcadena incompleta
           subcads = new String[div];
       }
       
       for (int i = 0; i < div; i++) {
           subcads[i] = cad.substring((i * cadlength), ((i + 1)* cadlength));
       }
       
       return subcads;
   }
} 
