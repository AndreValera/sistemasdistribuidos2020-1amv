/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemas2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Iterator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author yonda
 */
public class GetClima {
    
    private ArrayList<String> datos;
    
    public GetClima() {
        datos = new ArrayList();
        try {
            Document doc = Jsoup.connect("https://weather.com/es-MX/tiempo/hoy/l/6121681b2c5df01145b9723d497c595cdf6c6295156f0d2eeac83bd0dfe90f0e").timeout(6000).get();
            
            Element blockw = doc.getElementById("hero-left-Nowcard-effdc055-bb06-4a40-9cc0-5b38015d6736");
            
            //Hacemos el scraping
            datos.add(getWeather(blockw));
            datos.add(getHumidity(blockw));
            datos.add(getWind(blockw));

               
        } catch (IOException ex) {
            datos.add("NODATA");
            datos.add("NODATA");
            datos.add("NODATA");
        }
    }
    
    private String getUpdate(Element aux) {
        String update = "NULL";
        
        Elements datosB = aux.getElementsByClass("loc-container");
        for (Element d1 : datosB.select("p.today_nowcard-timestamp")) {
            update = d1.text();
        }
        return update;
    }
    
    private String getWeather(Element aux) {
        String weat = "NULL";
        
        Elements datosB = aux.getElementsByClass("today_nowcard-section today_nowcard-condition");
        for (Element d1 : datosB.select("div.today_nowcard-temp")) {
            weat = d1.text();
        }
        return weat;
    }
    
    private String getStatus(Element aux) {
        String status = "NULL";
        
        Elements estados = aux.getElementsByClass("today_nowcard-section today_nowcard-condition");
        for (Element d1 : estados.select("div.today_nowcard-phrase")) {
            status = d1.text();
        }
        return status;
    }
    
    private String getFeels(Element aux) {
        String feels = "NULL";
        
        Elements estados = aux.getElementsByClass("today_nowcard-section today_nowcard-condition");
        for (Element d1 : estados.select("div.today_nowcard-feels")) {
            feels = d1.text();
        }
        return feels;
    }
    
    private String getHumidity(Element aux) {
        String hum = "NULL";
        Iterator<Element> ite = null;
        
        Elements estados = aux.getElementsByClass("today_nowcard-sidecar component panel");
        for (Element d1 : estados.select("tbody")) {
            ite = d1.select("td").iterator();
        }
        
        ite.next();
        hum = ite.next().text();
        
        return hum;    
    }
    
    private String getWind(Element aux) {
        String wind = "NULL";
        Iterator<Element> ite = null;
        
        Elements estados = aux.getElementsByClass("today_nowcard-sidecar component panel");
        for (Element d1 : estados.select("tbody")) {
            ite = d1.select("td").iterator();
        }
        
        wind = ite.next().text();
        
        return wind;    
    }

    /**
     * @return the datos
     */
    public ArrayList<String> getDatos() {
        return datos;
    }

    /**
     * @param datos the datos to set
     */
    public void setDatos(ArrayList<String> datos) {
        this.datos = datos;
    }
    
}
