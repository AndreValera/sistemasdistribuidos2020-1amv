/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemas2;

import java.io.IOException;
import java.text.SimpleDateFormat;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author yonda
 */
public class GetDolarInfo {
    private String v1;
    private String c1;
    
    public GetDolarInfo() {
        v1 = "NULL";
        c1 = "NULL";
        
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
 
        try {
            Document doc = Jsoup.connect("https://mx.investing.com/currencies/usd-mxn").timeout(6000).get();
            Element block = doc.getElementById("quotes_summary_secondary_data");
            Elements datos = block.getElementsByClass("bottomText float_lang_base_1");
            
            for (Element venta: datos.select("span.inlineblock.pid-39-ask")) {
                v1 = venta.text();
            }
            
            for (Element compra: datos.select("span.inlineblock.pid-39-bid")) {
                c1 = compra.text();
            }
            
        } catch (IOException ex) {
            System.out.println("Error al conectar con el sitio");
            c1 = "NODATA";
            v1 = "NODATA";
        }
    }

    /**
     * @return the v1
     */
    public String getV1() {
        return v1;
    }

    /**
     * @param v1 the v1 to set
     */
    public void setV1(String v1) {
        this.v1 = v1;
    }

    /**
     * @return the c1
     */
    public String getC1() {
        return c1;
    }

    /**
     * @param c1 the c1 to set
     */
    public void setC1(String c1) {
        this.c1 = c1;
    }
    
}
