package sistemas2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class TestSleep {
    
    String PIN1,PIN2;
    
    TestSleep(){     
        PIN1=null;
        PIN2=null;
    }
    
     TestSleep(String contraseña){     
        PIN1=contraseña;
        PIN2=contraseña;
    }
    
    void doTask() throws IOException, InterruptedException{
        BufferedReader DatosTeclado = new BufferedReader ( new InputStreamReader (System.in)); 
        PrintWriter escritor = null; 
        BufferedReader entrada=null;
        String DatosEnviados = null;
        Scanner teclado = new Scanner(System.in);
        String contr;
        String maquina;
        Socket cliente = null; 
	int puerto; 
        
        maquina = "localhost"; 
	puerto = 12345;
                
        System.out.println("Iniciando Main Controlador ...");        
        System.out.println("Presione cualquier tecla para continuar");
        
        Contador mst = new Contador(this, 30000);
        DatosEnviados = DatosTeclado.readLine();
        mst.start();
        
        Thread.sleep(500);
    
        System.out.println ("Ingrese el código de verificación");   
        contr = DatosTeclado.readLine();
        Thread.sleep(500);
   
        do{
        if(PIN1!=PIN2)
            break;
        if(contr.equals(PIN1))
            break;
         
        else{
        System.out.println("Introduzca de nuevo el CODIGO");
        contr = teclado.nextLine();
                 Thread.sleep(500);
               }
        
        }while(!contr.equals(PIN1)||PIN1!=PIN2);
    
        if(contr.equals(PIN1)){  
         try{ 
              cliente = new Socket (maquina,puerto); 
		        }catch (Exception e){ 
			System.out.println ("Fallo : "+ e.toString()); 
			System.exit (0); 
		    }
         try{ 
			escritor = new PrintWriter(cliente.getOutputStream(), true);
			entrada=new BufferedReader(new InputStreamReader(cliente.getInputStream()));
 
		}catch (Exception e){ 
			System.out.println ("Fallo : "+ e.toString()); 
			cliente.close(); 
			System.exit (0); 
		} 
         String line;
                char [] linea;
                
		System.out.println("Conectado con el Servidor. Listo para enviar datos...");
		
		do{ 
			DatosEnviados = DatosTeclado.readLine(); 
			escritor.println (DatosEnviados); 
			line = entrada.readLine();
                        //linea=line.toCharArray();                        
			System.out.println(line);           
		}while (!DatosEnviados.equals("FIN")); 
                System.out.println ("Finalizada conexion con el servidor"); 
		try{ 
			escritor.close(); 
		}catch (Exception e){}
        }
        
        else 
            System.out.println("EL PIN YA NO ES VÁLIDO");
    }
           
            
  
}

